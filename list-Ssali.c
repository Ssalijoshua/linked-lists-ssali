#include <stdio.h>
#include <stdlib.h>

typedef struct Node
{
    int number;
    struct Node *next;
} Node;

// Function prototypes
Node *createNode(int num) {
    Node *result = malloc(sizeof(Node));
    result->number = num;
    result->next = NULL;
    return result;
}

void printList(Node *head) {
    Node *temporary = head;
    while (temporary != NULL) {
        printf("%d\n", temporary->number);
        temporary = temporary->next;
    }
}

void append(Node **head, int num) {
    Node *newnode = createNode(num);
    if (*head == NULL) {
        *head = newnode;
        return;
    }
    Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newnode;
}
// int* x;

//int y = 23; 

// x=&y;
// 
void prepend(Node **head, int num) {
    Node *newnode = createNode(num);
    newnode->next = *head;
    *head = newnode;
}

void deleteByKey(Node **head, int key) {
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    Node *temp = *head;
    Node *prev;

    if (key == 1) {
        *head = temp->next;
        free(temp);
        return;
    }

    for (int i = 1; temp != NULL && i < key; i++) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) {
        printf("Key not found.\n");
        return;
    }

    prev->next = temp->next;
    free(temp);
}

void deleteByValue(Node **head, int value) {
    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    Node *temp = *head;
    Node *prev = NULL;

    // if (temp != NULL && temp->number == value) {
    //     *head = temp->next;
    //     free(temp);
    //     return;
    // }

    while (temp != NULL && temp->number != value) {
        prev = temp;
        temp = temp->next;
    }

    if (temp == NULL) {
        printf("Value not found.\n");
        return;
    }

    prev->next = temp->next;
    free(temp);
}

void insertByKey(Node **head, int key, int value) {
    if (*head == NULL || key == 1) {
        prepend(head, value);
        return;
    }

    Node *newnode = createNode(value);
    Node *temp = *head;

    for (int i = 1; temp != NULL && i < key - 1; i++) {
        temp = temp->next;
    }

    if (temp == NULL) {
        printf("Key not found.\n");
        return;
    }

    newnode->next = temp->next;
    temp->next = newnode;
}

void insertByValue(Node **head, int searchValue, int newValue) {
    Node *newnode = createNode(newValue);

    if (*head == NULL) {
        printf("List is empty.\n");
        return;
    }

    Node *temp = *head;

    while (temp != NULL && temp->number != searchValue) {
        temp = temp->next;
    }

    if (temp == NULL) {
        printf("Value not found.\n");
        return;
    }

    newnode->next = temp->next;
    temp->next = newnode;
}

int main() {
    Node *head = NULL;
    int choice, data;

    Node *node1 = createNode(1);
    head = node1;

    Node *node2 = createNode(2);
    node1->next = node2;

    Node *node3 = createNode(3);
    node2->next = node3;

    Node *node4 = createNode(4);
    node3->next = node4;

    Node *node5 = createNode(5);
    node4->next = node5;

    while (1) {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Insert\n");
        printf("5. Delete\n");
        printf("6. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

        switch (choice) {
            case 1:
                printList(head);
                break;
            case 2:
                printf("Enter data to append: ");
                scanf("%d", &data);
                append(&head, data);
                printList(head);
                break;
            case 3:
                printf("Enter data to prepend: ");
                scanf("%d", &data);
                prepend(&head, data);
                printList(head);
                break;
            case 4:
                printf("1. Insert by value\n");
                printf("2. Insert by key\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);

                switch (choice) {
                    case 1:
                        printf("Enter value to insert after: ");
                        scanf("%d", &data);
                        printf("Enter data to insert: ");
                        scanf("%d", &choice);
                        insertByValue(&head, data, choice);
                        printList(head);
                        break;
                    case 2:
                        printf("Enter key to insert after: ");
                        scanf("%d", &data);
                        printf("Enter data to insert: ");
                        scanf("%d", &choice);
                        insertByKey(&head, data, choice);
                        printList(head);
                        break;
                    default:
                        printf("Invalid choice.\n");
                        break;
                }
                break;
            case 5:
                printf("1. Delete by value\n");
                printf("2. Delete by key\n");
                printf("Enter your choice: ");
                scanf("%d", &choice);

                switch (choice) {
                    case 1:
                        printf("Enter data to delete: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        printList(head);
                        break;
                    case 2:
                        printf("Enter key to delete: ");
                        scanf("%d", &data);
                        deleteByKey(&head, data);
                        printList(head);
                        break;
                    default:
                        printf("Invalid choice.\n");
                        break;
                }
                break;
            case 6:
                return 0;
            default:
                printf("Invalid choice. Please try again.\n");
        }
    }

    return 0;
}
